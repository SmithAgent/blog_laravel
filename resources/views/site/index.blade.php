@extends('layouts.main')

@section('header')
        <!-- Page Header -->
        <!-- Set your background image for this header on the line below. -->
        <header class="intro-header" style="background-image: url('images/home-bg.jpg')">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                        <div class="site-heading">
                            <h1>Тестовый блог</h1>
                            <hr class="small">
                            <span class="subheading">Тестовый блог на божественном фреймворке</span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
@endsection

@section('content')
        <!-- Main Content -->
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
           
                @foreach($articles as $article)
                    <div class="post-preview">
                        <a href="{{action('FrontController@show',['id'=>$article->id])}}">
                            <h2 class="post-title">
                                {{$article->title}}
                            </h2>
                            <h3 class="post-subtitle">
                                {!!str_limit($article->content, 250)!!}
                            </h3>
                        </a>
                        <p class="post-meta">Posted by <a href="#">{{$article->user['name']}}</a> {{$article->updated_at}}</p>
                    </div>
                    <hr>
                @endforeach
                
                    <!-- Pager -->
                    <ul class="pager">
                        <li class="next">
                            <a href="#">Older Posts &rarr;</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <hr>

  @endsection