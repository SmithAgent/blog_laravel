@extends('layouts.main')

@section('header')
<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('../../images/post-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="post-heading">
                    <h1>{{$article->title}}</h1>
                    <h2 class="subheading">Тут можно было бы впендюрить описание</h2>
                    <span class="meta">Опубликовал <a href="#">{{$name}}</a> {{$article->updated_at}}</span>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection

@section('content')
<!-- Post Content -->
<article>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                {!!$article->content!!}
            </div>
        </div>
    </div>
</article>
<div class="container">
    <div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        <div class="comments">
            <ul>
                @foreach($comments as $comment)
                <li>Автор: {{$comment->author}}<br>{{$comment->content}}</li>
                @endforeach
            </ul>
        

        @if($article->comments_enable)
            @include('site.comment')
        @endif
        </div>
    </div>
</div>
</div>

<hr>
@endsection
