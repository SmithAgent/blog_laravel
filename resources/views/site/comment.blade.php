
@if(Session::has('message'))
<div class="alert alert-success">
    {{Session::get('message')}}
</div>
@endif

@if (count($errors) > 0) <!-- проверяет ошибки сессионные и выводит-->
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif 
<form method="post" name="sentMessage" id="contactForm" novalidate><!--контроллер обработки описан в роутах - там Route::post('/show/{id}','CommentsController@save') вызывается обработка -->
    <div class="row control-group">
        <div class="form-group col-xs-12 floating-label-form-group controls">
            <label>Имя</label>
            <input type="text" class="form-control" placeholder="Имя" id="name" name="author" required data-validation-required-message="Пожалуйста, введите Ваше имя.">
            <p class="help-block text-danger"></p>
        </div>
    </div>
    <div class="row control-group">
        <div class="form-group col-xs-12 floating-label-form-group controls">
            <label>Email</label>
            <input type="email" class="form-control" placeholder="Email" id="email" name="email" required data-validation-required-message="Пожалуйста, введите Ваш email адресс.">
            <p class="help-block text-danger"></p>
        </div>
    </div>
    <div class="row control-group">
        <div class="form-group col-xs-12 floating-label-form-group controls">
            <label>Комментарий</label>
            <textarea rows="5" class="form-control" placeholder="Комментарий" id="message" name="content" required data-validation-required-message="Пожалуйста, введите Ваш комментарий."></textarea>
            <p class="help-block text-danger"></p>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-12">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <button type="submit" class="btn btn-default">Оставить</button>
        </div>
    </div>

</form>
