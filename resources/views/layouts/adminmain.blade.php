<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">


        <title>Админка</title>

        <!-- Bootstrap core CSS -->
        <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="{{asset('css/admin.css')}}" rel="stylesheet">

        <script src="{{asset('js/jquery-3.1.1.js')}}"></script>

        <script src="{{asset('js/tinymce/tinymce.min.js')}}"></script>
        <script>
            tinymce.init({
                plugins: "image",
                selector: '#editor',
                file_browser_callback : elFinderBrowser
            });

            function elFinderBrowser (field_name, url, type, win) {
                tinymce.activeEditor.windowManager.open({
                    file: '/admin/elfinder',// use an absolute path!
                    title: 'elFinder 2.0',
                    width: 900,
                    height: 450,
                    resizable: 'yes'
                    }, {
                        setUrl: function (url) {
                            win.document.getElementById(field_name).value = url;
                        }
                });
                return false;
            }
        </script>

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">
                        <li class="active"><a href="/admin/articles">Все статьи</a></li>
                        <li><a href="/admin/articles/create">Добавить статью</a></li>
                    </ul>
                    
                    @can('admin_access')
                    <ul class="nav nav-sidebar">
                        <li><a href="/admin/categories">Все категории</a></li>
                        <li><a href="/admin/categories/create">Добавить категорию</a></li>
                    </ul>
                    
                    <ul class="nav nav-sidebar">
                        <li><a href="/admin/comments">Все комментарии</a></li>
                    </ul>
                    
                    <ul class="nav nav-sidebar">
                        <li><a href="/admin/elfinder">Медиа</a></li>
                    </ul>
                    <ul class="nav nav-sidebar">
                        <li><a href="/admin/pages">Все страницы</a></li>
                        <li><a href="/admin/pages/create">Добавить страницу</a></li>
                    </ul>
                    @endcan
					<ul class="nav nav-sidebar">
                        <li><a href="{{ url('/logout') }}" onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">Выход</a></li>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                    </form>
                    </ul>
                </div>
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    @yield('content')
                </div>
            </div>
        </div>


    </body>
</html>

