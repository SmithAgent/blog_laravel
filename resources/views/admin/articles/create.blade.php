@extends('layouts.adminmain')
@section('content')

@if(Session::has('message'))
<div class="alert alert-success">
    {{Session::get('message')}}
</div>

@endif 

@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form action="{{action('ArticlesController@store')}}" method="post" enctype="multipart/form-data"><!--enctype для file -->
    Превью:<br>
    <input type="file" class="btn btn-default btn-file" name="preview"><br>
    <div class="form-group">
        <label>Название статьи::</label>
        <input type="text" class="form-control" name="title" value="{{old('title')}}">
    </div>
    <div class="form-group">
        <label>Текст статьи:</label>
        <textarea class="form-control" rows="5" name="content" id="editor" value="{{old('content')}}"></textarea>
    </div>
    <div class="form-group">
        <label>Категория:</label>
        <select class="form-control" name="category_id">

            @foreach($categories as $category)
            <option value="{{$category->id}}">{{$category->title}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>Разрешить комментарии?</label>
        <select class="form-control" name="comments_enable">
            <option selected="selected" value="1">Да</option>
            <option value="0">Нет</option>

        </select>
    </div>


    <div class="form-group">
        <label>Опубликовать?</label>
        <select class="form-control" name="public">
            <option selected="selected" value="1">Да</option>
            <option value="0">Нет</option>
        </select>
    </div>

    <div class="form-group">
        <label>description:</label>
        <input type="text" class="form-control" name="meta_description" value="{{old('meta_description')}}">
    </div>
    <div class="form-group">
        <label>keywords:</label>
        <input type="text" class="form-control" name="meta_keywords" value="{{old('meta_keywords')}}>
    </div>
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <input type="submit" value="Сохранить">
</form>
@endsection

