@extends('layouts.adminmain')
@section('content')

<h2 class="sub-header">Статьи</h2>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Id</th>
                <th>Название</th>
				<th>Просмотров</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>

            @foreach($articles as $article)
            <tr>
                <td>{{$article->id}}</td>
                <td>{{$article->title}}</td>
				<td>{{$views[$article->id]}}</td>
                <td>
                    @if($access[$article->id])
                        <a href="{{action('ArticlesController@edit',['article_id' => $article->id])}}" class="btn btn-default">Изменить</a>
                    @endif
                </td>
                <td>
                    <form method="POST" action="{{action('ArticlesController@destroy',['article_id'=>$article->id])}}">
                        <input type="hidden" name="_method" value="delete"/>
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input type="submit" class="btn btn-danger" value="Удалить"/>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection
