@extends('layouts.adminmain')
@section('content')

<h2 class="sub-header">Категории</h2>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Id</th>
                <th>Название</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>

            @foreach($categories as $category)
            <tr>
                <td>{{$category->id}}</td>
                <td>{{$category->title}}</td>
                <td><a href="{{action('CategoriesController@edit',['category_id' => $category->id])}}" class="btn btn-default">Изменить</a></td>
                <td>
                    <form method="POST" action="{{action('CategoriesController@destroy',['category_id'=>$category->id])}}">
                        <input type="hidden" name="_method" value="delete"/>
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input type="submit" class="btn btn-danger" value="Удалить"/>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

    
@endsection

<?php if(0){ //попробывать?>
<script type="text/javascript">
$('document').ready(function(){
   $('.del').click(function(){
       parent = $(this).parent().parent();//tr
       id = parent.children().first().html();//id из 1-й ячейки, потом надо будет передать более красивее, может через data
       confirm = confirm('Действительно удалить?');//надо будет потом попап сделать на подтверждение
       if(!confirm) return false;
       
       $.ajax({
           url:'admin/categories'+id,
           method:'delete',
           data:{'_token':"{{csrf_token()}}"},
           success:function(msg){
               parent.remove();
               alert('Категория '+msg+' удалена');
           },
           error:function(msg){
               console.log(msg);
           }
       });
       
   }); 
});
</script>
<?php } ?>
