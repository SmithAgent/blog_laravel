@extends('layouts.adminmain')
@section('content')

@if(Session::has('message'))
<div class="alert alert-success">
    {{Session::get('message')}}
</div>
@endif 
@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<form action="{{action('CategoriesController@update',['category'=>$category->id])}}" method="post">

    <div class="form-group">
        <label>Название категории:</label>
        <input type="text" class="form-control" name="title" value="{{old('title') ? old('title') : $category->title}}">
    </div>
    <input type="hidden" name="_method" value="put"/>
    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
    <input type="submit" class="btn btn-default" value="Сохранить">
</form>
@endsection