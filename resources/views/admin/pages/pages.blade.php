@extends('layouts.adminmain')
@section('content')

<h2 class="sub-header">Страницы</h2>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Id</th>
                <th>Название</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        
            @foreach($pages as $page)
            <tr>
                <td>{{$page->id}}</td>
                <td>{{$page->title}}</td>
                <td><a href="{{action('PagesController@edit',['page_id' => $page->id])}}" class="btn btn-default">Изменить</a></td>
                <!--<td><span class="del">Удалить</span></td>--><!--ajax-->
                <td><form method="POST" action="{{action('PagesController@destroy',['page_id'=>$page->id])}}">
                        <input type="hidden" name="_method" value="delete"/>
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input type="submit" class="btn btn-danger" value="Удалить"/>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection