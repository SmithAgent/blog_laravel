@extends('layouts.adminmain')
@section('content')

@if(Session::has('message'))
<div class="alert alert-success">
    {{Session::get('message')}}
</div>
@endif 
<form action="{{action('PagesController@store')}}" method="post">
    <div class="form-group">
        <label>Название страницы:</label>
        <input type="text" class="form-control" name="title">
    </div>
    <div class="form-group">
        <label>Содержимое:</label>
        <textarea class="form-control" rows="5" name="content" id="editor"></textarea>
    </div>
    <div class="form-group">
        <label>Описание:</label>
        <input type="text" class="form-control" name="meta_description">
    </div>
    <div class="form-group">
        <label>Ключевые слова:</label>
        <input type="text" class="form-control" name="meta_keywords">
    </div>
    <div class="form-group">
        <label>Опубликовать?</label>
        <select class="form-control" name="public">
            <option selected="selected" value="1">Да</option>
            <option value="0">Нет</option>
        </select>
    </div>

    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <input type="submit" value="Сохранить">
</form>

@endsection