@extends('layouts.adminmain')
@section('content')

@if(Session::has('message'))
<div class="alert alert-success">
    {{Session::get('message')}}
</div>
@endif 
<form action="{{action('PagesController@update',['page'=>$page->id])}}" method="post">

    <div class="form-group">
        <label>Название страницы:</label>
        <input type="text" class="form-control" name="title" value="{{$page->title}}">
    </div>

    <div class="form-group">
        <label>Содержимое:</label>
        <textarea class="form-control" rows="5" name="content" id="editor">{{$page->content}}</textarea>
    </div>

    <div class="form-group">
        <label>Описание:</label>
        <input type="text" class="form-control" name="meta_description" value="{{$page->meta_description}}">
    </div>
    
    <div class="form-group">
        <label>Ключевые слова:</label>
        <input type="text" class="form-control" name="meta_keywords" value="{{$page->meta_keywords}}">
    </div>
    
    <div class="form-group">
        <label>Опубликовать?</label>
        <select class="form-control" name="public">
            @if($page->public)
            <option selected="selected" value="1">Да</option>
            <option value="0">Нет</option>
            @else
            <option value="1">Да</option>
            <option selected="selected" value="0">Нет</option>
            @endif
        </select>
    </div>

    <input type="hidden" name="_method" value="put"/>
    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
    <input type="submit" class="btn btn-default" value="Сохранить">
</form>
@endsection