<!DOCTYPE html>
<html>
<head>
<meta charaset="utf-8"/>
<title>Админка</title>
<link rel="stylesheet" href="{{asset('css/admin.css')}}">
<script src="{{asset('js/jquery-3.1.1.js')}}"></script>

<script src="{{asset('js/tinymce/tinymce.min.js')}}"></script>
<script>
tinymce.init({
    plugins: "image",
    selector: '#editor',
    file_browser_callback : elFinderBrowser
});

function elFinderBrowser (field_name, url, type, win) {
     tinymce.activeEditor.windowManager.open({
     file: '/admin/elfinder',// use an absolute path!
     title: 'elFinder 2.0',
     width: 900,
     height: 450,
     resizable: 'yes'
    }, {
     setUrl: function (url) {
     win.document.getElementById(field_name).value = url;
    }
});
return false;
}
</script>

</head> 
<body> 
<div id="header">
<h1>Админка</h1>
 <div id="content">@yield('content')</div>
 </body>
</html>
