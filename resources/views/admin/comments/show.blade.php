@extends('layouts.adminmain')
@section('content')

<h2 class="sub-header">Категории</h2>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <td>Статья</td>
                <td>Автор</td>
                <td>Email</td>
                <td>Комментарий</td>
                <td>Дата</td>
                <td>Статус</td>

            </tr>
        </thead>
        <tbody>

            @foreach($comments as $comment)
            <tr>
                <td>{{$comment->article->title}}</td>
                <td>{{$comment->author}}</td>
                <td>{{$comment->email}}</td>
                <td>{{$comment->content}}</td>
                <td>{{$comment->created_at}}</td>
                <td>{{$comment->public}}</td>
                <td>
                    <a href="{{action('CommentsController@published',['id'=>$comment->id])}}" class="btn btn-default">Опубликовать</a>
                </td>
                <td><a href="{{action('CommentsController@delete',['id'=>$comment->id])}}" class="btn btn-danger">Удалить</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
