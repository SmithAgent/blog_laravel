<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index');
Route::get('/show/{id}','FrontController@show');
Route::post('/show/{id}','CommentsController@save');
Route::get('/pages/show/{id}','PagesController@show');

Auth::routes();//регистрация страниц аутентификации,регистрации,логирования

Route::group(['prefix' => 'admin','middleware' => ['web','auth']], function() {

    Route::get('/','ArticlesController@index');

    Route::resource('articles','ArticlesController');
    Route::resource('pages','PagesController');
    Route::resource('categories','CategoriesController');

    Route::get('elfinder',function(){
        return view('admin.elfinder');
    });

    Route::get('connector','Elfinder\ElfinderController@connector');
    Route::post('connector','Elfinder\ElfinderController@connector');

    Route::get('comments','CommentsController@show');
    Route::get('comments/delete/{id}','CommentsController@delete');
    Route::get('comments/published/{id}','CommentsController@published');
	
	
});


Route::get('/home', 'HomeController@index');
