<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        
        Gate::define('admin_access',function($user){
            return $user->group->id == 1;
        });
        
        Gate::define('update_article',function($user,$article){
            if($user->group->id == 1){
                return true;
            }
            elseif($user->id == $article->user_id){
                return true;
            }else{
                return false;
            }
        });
        //
    }
}
