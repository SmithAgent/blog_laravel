<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redis;
use Cache;
use App\Article;
use App\User;

use DB;

class FrontController extends Controller
{
    public function index(){
		//DB::connection()->enableQueryLog();
        $articles = Cache::remember('blog_posts_cache',1,function(){
			return Article::where('public','=',1)->get();
		});
		
		//$log = DB::getQueryLog();
		//print_r($log);
    
        return view('site.index',['articles' => $articles]);
    }
    
    public function show($id){
				
        $article = Article::where('public','=',1)->find($id);

        $name = $article->user->name;
        $comments = $article->comments()->where('public','=',1)->get();
		
		$redis = Redis::connection();
		$redis->incr('article:'.$id.':views');

        return view('site.show',['article' => $article,'comments' => $comments,'name' => $name]);
    }
}
