<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Comment;

class CommentsController extends Controller
{
    //

    public function __construct(){
        //$this->middleware('auth',['only' => ['show']]);
    }


    public function save(Request $request,$id){

        $messages = array(
            'required' => 'Поле :attribute должно быть заполнено.',
            'min' => 'Поле :attribute должно  должно быть не меньше :min .',
            'max' => 'Поле :attribute должно  должно быть не меньше :max .',
            'email' => 'Поле :attribute должно быть адрессом електронной почты'
            
        );
        
        $this->validate($request,[
            'author' => 'required|min:5|max:100',
            'email' => 'required|email',
            'content' => 'required|min:5|max:400'
            ],$messages);

        $data = $request->all();
        $data['article_id'] = $id;//добавляем в массив id статьи
        Comment::create($data);

        return back()->with('message','Спасибо за комментарий');
    }

    public function show(){

        $comments = Comment::all();

        return view('admin.comments.show',['comments' => $comments]);
    }

    public function delete($id){
        $comment = Comment::find($id);
        $comment->delete();

        return back();
    }

    public function published($id){
        $comment = Comment::find($id);
        $comment->public = 1;
        $comment->save();

        return back();
    }


}
