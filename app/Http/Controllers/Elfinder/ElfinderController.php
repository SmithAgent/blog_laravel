<?php

namespace App\Http\Controllers\Elfinder;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ElfinderController extends Controller
{
    //
    public function connector() {
        require 'ElfinderConnector.php';
    }
}
