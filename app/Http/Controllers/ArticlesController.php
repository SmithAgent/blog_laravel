<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Category;
use App\Article;
use Gate;
use Validator;

use Illuminate\Support\Facades\Redis;

class ArticlesController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        //        
        $articles = Article::all();

        //создам массив возможности редактирования, ключами будет id статьи, а значением - возможность редактирования данным пользователем
        $access = array();
		$views = array();
		
		$redis = Redis::connection();

        foreach($articles as $article){
            $access[$article->id] = Gate::allows('update_article',$article) ? 1 : 0;
			$views[$article->id] = $redis->get('article:'.$article->id.':views');
        }

        return view('admin.articles.articles',['articles' => $articles,'access' => $access, 'views' => $views]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
        $categories = Category::all();//для селекта

        return view('admin.articles.create',['categories' => $categories]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        //
        $messages = array(
            'required' => 'Поле :attribute должно быть заполнено.',
            'min' => 'Поле :attribute должно  должно быть не меньше :min .',
            'max' => 'Поле :attribute должно  должно быть не меньше :max .'

        );

        $this->validate($request, [
            'title' => 'required|min:20|max:100',
            'content' => 'required|min:20|max:500',
            'meta_description' => 'required|min:20|max:40',
            'meta_keywords' => 'required|min:20|max:40',
            ],$messages);


        $data = $request->all();//все данные из формы
        $data['user_id'] = Auth::user()->id;

        if($request->has('preview')){//если есть картинка

            $date=date('d.m.Y');//дата для имени каталога
            $root=$_SERVER['DOCUMENT_ROOT']."/images/";//папка для загрузки картинок

            if(!file_exists($root.$date)){//если папки с данной датой нет, то создадим ее
                mkdir($root.$date);
            }

            $file_name = $request->file('preview')->getClientOriginalName();//получу имя файла
            $request->file('preview')->move($root.$date,$file_name);//перемещаем файл в папку с оригинальным именем

            $data['preview']="/images/".$date."/".$file_name;// меняем значение preview на нашу ссылку

            Article::create($data); 

        }else{//иначе сохраняем как есть нах
            Article::create($data);
        }

        return back()->with('message','Статья добавлена');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
        $article = Article::find($id);

        $categories = Category::all();//для селекта

        return view('admin.articles.edit',['article' => $article,'categories' => $categories]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        //
        $messages = array(
            'required' => 'Поле :attribute должно быть заполнено.',
            'min' => 'Поле :attribute должно  должно быть не меньше :min .',
            'max' => 'Поле :attribute должно  должно быть не больше :max .'

        );

        $this->validate($request, [
            'title' => 'required|min:20|max:100',
            'content' => 'required|min:20|max:800',
            'meta_description' => 'required|min:20|max:40',
            'meta_keywords' => 'required|min:20|max:40',
            ],$messages);

        $article = Article::find($id);
        $article->update($request->all());
        $article->save();

        return back()->with('message','Статья обновлена');

    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        //
        $article = Article::find($id);
        $article->delete();

        return back()->with(['message'=>"Статья ".$article->title." удалена"]);
    }
}
