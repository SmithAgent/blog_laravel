<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;

class CategoriesController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        //
        $categories = Category::all();

        return view('admin.categories.categories',['categories' => $categories]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //

        return view('admin.categories.create');

    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        //
        $messages = array(
            'required' => 'Поле :attribute должно быть заполнено.',
            'max' => 'Поле :attribute должно  должно быть не больше :max .'

        );

        $this->validate($request, [
            'title' => 'required|max:100',
            ], $messages);

        Category::create($request->all());
        return back()->with('message','Категория была добавлена');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
        $category = Category::find($id);

        return view('admin.categories.edit',['category' => $category]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        //
        $messages = array(
            'required' => 'Поле :attribute должно быть заполнено.',
            'max' => 'Поле :attribute должно  должно быть не больше :max .'

        );

        $this->validate($request, [
            'title' => 'required|max:100',
            ], $messages);


        $category = Category::find($id);
        $category->update($request->all());
        $category->save();

        return back()->with('message','Категория обновлена');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        //ajax
        /*$category = Category::find($id);
        $category->delete();

        return $category->title;*/

        //по умолчанию с помощью формы
        $category=Category::find($id); 
        $category->delete();
        return back()->with(['message'=>"Категория ".$category->title." удалена"]);
    }
}
